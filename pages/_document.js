import Document, { Html, Head, Main, NextScript } from 'next/document';
import React from 'react';

export default class MyDocument extends Document {
  render() {
    return (
      <Html lang="en">
        <Head>
          <title>EXSOLUTE | To make your business future-ready</title>
          <meta name="viewport" content="initial-scale=1.0, width=device-width" />
          <meta
            name="description"
            content="We at EXSOLUTE are focused on building right tech solutions, right way and FAST. We value the investments you have made on your business and your plans for the future which makes us think and build, instead of just building something."
          />
          <meta
            name="keywords"
            content="build software, mobile apps, website, e-commerce, enterprise software, cloud software, tech solutions, agile development, software project, start up, MVP, Minimum viable product, scalable software, optimal solution, scrum, kanban, build right first time, saas, software as a service, user friendly, user experience, maintainable, secure, reliable, information security, quality software, technology partner, software company"
          />
          <script async src={`https://www.googletagmanager.com/gtag/js?id=${process.env.GOOGLE_ANALYTICS_ID}`} />
          <script
            // eslint-disable-next-line react/no-danger
            dangerouslySetInnerHTML={{
              __html: `
              window.dataLayer = window.dataLayer || [];
              function gtag(){dataLayer.push(arguments);}
              gtag('js', new Date());
              gtag('config', '${process.env.GOOGLE_ANALYTICS_ID}');
            `
            }}
          />
        </Head>
        <body data-bs-spy="scroll" data-bs-target="#navbar" data-bs-offset="0">
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}
