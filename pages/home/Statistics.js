/* eslint-disable jsx-a11y/heading-has-content */
import { memo, useCallback, useState } from 'react';
import CountUp from 'react-countup';
import OnVisible from 'react-on-visible';

const stats = [
  { id: 1, number: 3, decimal: 0, title: 'Happy clients' },
  { id: 2, number: 9.8, decimal: 1, title: 'Overall client satisfaction level' },
  { id: 3, number: 2, decimal: 0, title: 'Active partnerships' },
  { id: 4, number: 9.9, decimal: 1, title: 'Overall employee satisfaction level' }
];

const Statistics = () => {
  const [visibleActive, setVisibleActive] = useState(false);

  const setVisibleSensorInactive = useCallback(isVisible => {
    if (isVisible) {
      setVisibleActive(true);
    }
  }, []);

  return (
    <section className="section">
      <div className="cover container">
        <div className="content text-left pt-3">
          <h1>A few numbers we are proud of</h1>
          <p>
            We measure our success by how well we satisfy our clients as well as our employees. We sure want to add you
            to these numbers
          </p>
        </div>
      </div>

      <section className="section-2 container-fluid p-0">
        <div className="container-fluid text-center">
          <div className="numbers-wrap">
            <OnVisible
              className="numbers d-flex flex-md-row flex-wrap justify-content-center"
              onChange={setVisibleSensorInactive}
            >
              {visibleActive &&
                stats.map(stat => (
                  <div key={stat.id} className="rect">
                    <h1 className="stat-count">
                      <CountUp end={stat.number} decimal="." decimals={stat.decimal} />
                    </h1>
                    <p>{stat.title}</p>
                  </div>
                ))}
            </OnVisible>
          </div>
        </div>
      </section>
    </section>
  );
};

export default memo(Statistics);
