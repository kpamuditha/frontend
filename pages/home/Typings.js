import { memo, useCallback, useState } from 'react';
import Typist from 'react-typist';

const typings = [
  { id: 1, title: 'Build a web app;' },
  { id: 2, title: 'Build a web site;' },
  { id: 3, title: 'Build a mobile app;' },
  { id: 4, title: 'Get things streamlined;' },
  { id: 5, title: 'Manage your projects;' }
];

const cursor = {
  show: true,
  blink: true,
  element: '|'
};

const Typings = () => {
  const [count, setCount] = useState(1);

  const loop = useCallback(() => {
    setTimeout(() => {
      setCount(0);
      setCount(1);
    }, 500);
  }, []);

  return (
    count && (
      <span className="txt-rotate">
        <Typist avgTypingDelay={120} cursor={cursor} onTypingDone={() => loop()}>
          {typings.map(({ id, title }) => (
            <span key={id}>
              {title}
              <Typist.Backspace count={title.length} delay={1000} />
              <Typist.Delay ms={500} />
            </span>
          ))}
        </Typist>
      </span>
    )
  );
};

export default memo(Typings);
