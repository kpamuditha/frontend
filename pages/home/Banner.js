import { memo } from 'react';
import { Link } from 'react-scroll';

const Banner = () => (
  <section className="header hero">
    <div className="container">
      <div className="row">
        <div className="tag-line col-md-6 col-sm-12 pr-2">
          <h6>Ready to rumble with your new idea?</h6>
          <br />
          <div className="fader">
            <h1>
              <span>EXSOLUTE</span> is here to help you
            </h1>
            <p>
            We are a team that is ready to take the responsibility of bringing your gig from your head to a reality. Our team can do what you need done, in the way you wish and in the way it lasts.
            </p>
            <div className="text-center">
              <Link
                activeClass="active"
                className="scroll-link btn btn-dark px-5 py-2"
                to="get-in-touch"
                smooth
                duration={500}
              >
                GET IN TOUCH
              </Link>
            </div>
          </div>
        </div>
        <div className="col-md-6 d-none d-md-flex align-items-center">
          <img src="/images/banner-img.png" layout="fill" />
        </div>
      </div>
    </div>
  </section>
);

export default memo(Banner);
