import { memo } from 'react';
import { Carousel } from 'react-bootstrap';

const Testimonials = () => (
  <section className="section testimonials" id="why-us">
    <div className="testimonials-head container">
      <div className="content text-right pt-3">
        <h1>Testimonials</h1>
        <p>Looking forward to hearing a similar story from you</p>
      </div>
    </div>
    <section className="section-5 testimonials__carousel-wrapper">
      <Carousel>
        <Carousel.Item>
          <div className="container p-0">
            <div className="row">
              <div className="col-md-3 client">
                <img className="d-block w-100" src="/images/client1.png" />
              </div>
              <div className="col-md-9">
                <h3 className="testimonials__quote">
                  Getting to know EXSOLUTE not only got us a tech-partner but also a great idea generator to sustain
                  even during these hard days. We are trully grateful to them.
                </h3>
                <p className="testimonials__person text-right">Shehan Liyanage (CEO)</p>
              </div>
            </div>
          </div>
        </Carousel.Item>
        <Carousel.Item>
          <div className="container p-0">
            <div className="row">
              <div className="col-md-3 client">
                <img src="/images/client2.png" alt="" />
              </div>
              <div className="col-md-9">
                <h3 className="testimonials__quote">
                  EXSOLUTE got us covered! They built our resume management system and created an easy way of
                  communicating with our foreign agents instead of using emails.
                </h3>
                <p className="testimonials__person text-right">Shamalka (MD)</p>
              </div>
            </div>
          </div>
        </Carousel.Item>
      </Carousel>
    </section>
  </section>
);

export default memo(Testimonials);
