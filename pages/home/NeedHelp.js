import { memo, useState } from 'react';
import { Alert } from 'react-bootstrap';
import { Controller, useForm } from 'react-hook-form';
import PhoneInput from 'react-phone-input-2';
import 'react-phone-input-2/lib/style.css';
import Typings from './Typings';

const NeedHelp = () => {
  const {
    register,
    formState: { errors },
    handleSubmit,
    reset,
    control
  } = useForm();
  const [formErrors, setFormErrors] = useState({ success: false, erors: false });
  const [submitting, setSubmitting] = useState(false);

  const submit = data => {
    setFormErrors({ success: false, errors: false });
    setSubmitting(true);
    fetch('https://mr21zc3cy1.execute-api.us-east-2.amazonaws.com/default/exolute-contact-form', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    })
      .then(response => response.json())
      .then(reponse => {
        setSubmitting(false);
        const success = reponse.statusCode === 200;
        if (success) {
          reset();
          setFormErrors({ success: true, errors: false });
        } else {
          setFormErrors({ success: false, errors: true });
        }
      })
      .catch(() => {
        setSubmitting(false);
        setFormErrors({ success: false, errors: true });
      });
  };

  return (
    <section className="section" id="get-in-touch">
      <div className="cover container">
        <div className="content text-right pt-3">
          <h1>Let&apos;s talk!</h1>
          <p>Fill and submit this form, and we will get back to you</p>
        </div>
      </div>
      <section className="section-3">
        <div className="container">
          <div className="row">
            <div className="col-md-6 col-sm-12 d-none d-md-block">
              <h1>
                If you need help to
                <br />
                <Typings />
              </h1>
            </div>

            <div className="col-md-6 col-sm-12">
              <form
                method="POST"
                name="exsolute"
                onSubmit={handleSubmit(submit)}
                noValidate
                className={submitting ? 'form-disabled' : ''}
              >
                <div className="form-group">
                  <div className="form-grad">
                    <input
                      type="email"
                      className="form-control"
                      id="email"
                      name="email"
                      placeholder="Enter your email here"
                      {...register('email', { required: true, pattern: /^\S+@\S+$/i })}
                    />
                    {errors.email?.type && <span className="error-msg">Invalid email</span>}
                  </div>
                </div>

                <div className="form-group">
                  <div className="form-grad">
                    <Controller
                      name="contact_number"
                      control={control}
                      rules={{ required: true }}
                      render={({ field }) => <PhoneInput {...field} country="us" />}
                    />
                    {errors.contact_number?.type && <span className="error-msg">Invalid contact number</span>}
                  </div>
                </div>

                <div className="form-group">
                  <div className="form-grad select-wrapper">
                    <select
                      className="form-control"
                      id="topic"
                      name="topic"
                      placeholder="How do you think we can help you?"
                      {...register('topic')}
                    >
                      <option>How can we help you?</option>
                      <option>I want to build a website</option>
                      <option>I want to build a mobile app</option>
                      <option>I want to build an end to end system</option>
                      <option>I don&apos;t know, I need support to figure out</option>
                    </select>
                  </div>
                </div>

                <div className="form-group">
                  <div className="form-grad">
                    <textarea
                      className="form-control"
                      id="brief"
                      name="brief"
                      placeholder="Give us a briefing, so we will come to the same page in a shorter time"
                      rows="10"
                      {...register('brief', { required: true })}
                    />
                    {errors.brief?.type && <span className="error-msg">Please give us a brief.</span>}
                  </div>
                </div>

                {formErrors.errors && (
                  <Alert variant="danger" dismissible onClose={() => setFormErrors({ ...formErrors, errors: false })}>
                    <Alert.Heading>Oh snap! You got an error!</Alert.Heading>
                    <p>Please check the form again.</p>
                  </Alert>
                )}

                {formErrors.success && (
                  <Alert variant="success" dismissible onClose={() => setFormErrors({ ...formErrors, success: false })}>
                    <Alert.Heading>Thank you</Alert.Heading>
                    <p>We will get back to you!</p>
                  </Alert>
                )}

                <div className="text-center">
                  <button type="submit" className="btn btn-light px-5 py-3" disabled={submitting}>
                    Let&apos;s get started
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </section>
    </section>
  );
};

export default memo(NeedHelp);
