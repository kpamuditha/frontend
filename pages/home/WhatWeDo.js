import { memo } from 'react';

const WhatWeDo = () => (
  <section className="section" id="what-we-do">
    <div className="container section1-head text-left pt-3">
      <h1>What we do</h1>
      <p>Our focus is beyond building something that just works</p>
    </div>

    <section className="section-1 container">
      <div className="row text-center">
        <div className="col-md-4">
          <i className="fas fa-laptop-code fa-8x" />
          <h3 className="mb-4">Build tech solutions</h3>
          <p className="text-justify what-we-do-txt">
            Today most business problems can be solved with technology. In fact technology has created a lot of new
            dimensions for businesses to emerge. We are ready to be the people to help you solve your business problems
            and discover new dimensions for your business with our technical expertise.
          </p>
        </div>
        <div className="col-md-4">
          <i className="fas fa-shipping-fast fa-8x" />
          <h3 className="mb-4">Fastest as possible</h3>
          <p className="text-justify what-we-do-txt">
            Business is a land-grab where earliest usually wins. We understand this simple concept and our methodology
            is based on providing you with something that works ASAP and iterating on it by responding to feedback that
            is provided after using an actual product. This way we can ensure that we build exactly what you need and in
            case changes are required, do them under minimum cost.
          </p>
        </div>
        <div className="col-md-4">
          <i className="fas fa-users-cog fa-8x" />
          <h3 className="mb-4">In the optimal way</h3>
          <p className="text-justify what-we-do-txt">
            We ensure that the products we build are viable for today as well as tomorrow of your business. Our
            expectations are high in terms of usage and data volumes that our products would have to deal with, hence,
            we focus on building products that can sustain instead of just building something.
          </p>
        </div>
      </div>
    </section>
  </section>
);

export default memo(WhatWeDo);