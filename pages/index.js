import 'bootstrap/dist/css/bootstrap.css';
import 'react-typist/dist/Typist.css';
import '@fortawesome/fontawesome-free/css/all.min.css';
import Banner from './home/Banner';
import NeedHelp from './home/NeedHelp';
import Statistics from './home/Statistics';
import Testimonials from './home/Testimonials';
import WhatWeDo from './home/WhatWeDo';

const Home = () => (
  <>
    <Banner />
    <div className="main">
      <WhatWeDo />
      <Testimonials />
      <Statistics />
      <NeedHelp />
    </div>
  </>
);

export default Home;
