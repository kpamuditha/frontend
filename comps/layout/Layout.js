import React, { memo } from 'react';
import Footer from './Footer';
import TopBar from './TopBar';

const Layout = ({ children }) => (
  <div>
    <TopBar />
    <div className="content-wrapper">{children}</div>
    <Footer />
  </div>
);

export default memo(Layout);
