import { memo } from 'react';

const Footer = () => (
  <footer className="footer">
    <div className="container">
      <div className="row text-left">
        <div className="col-md-6 about">
          <h4 className="text-light mb-4">About EXSOLUTE</h4>
          <p className="text-justify about-text">
            <span>EXSOLUTE</span> is a technology services company based in Sri Lanka and specialized in building cloud
            based software (SaaS) and applications. The products built by <span>EXSOLUTE</span> are special mainly due
            to their validity across a longer term and multi-platform compatibility. This is a result of effort put on
            strategizing the products from the start. For us at <span>EXSOLUTE</span>, key is ensuring that you reach
            your goals.
          </p>
        </div>
        <div className="col-md-3">
          <h4 className="text-light">Our locations</h4>
          <p className="text-justify address-top">Head office </p>
          <p>
            No 22, <br />
            Pangiriwatte Mw, <br />
            Mirihana, Nugegoda. <br />
            PO 10250. <br />
            Sri Lanka
          </p>
        </div>
        <div className="col-md-3">
          <h4 className="text-light">Find us through</h4>
          <div className="col d-flex align-items-center my-3 px-0">
            <a href="https://www.facebook.com/Exsolute-109390257951535" target="_blank" rel="noreferrer">
              <i className="fab fa-facebook-f px-2" />
            </a>
            <a href="https://www.linkedin.com/company/exsolute" target="_blank" rel="noreferrer">
              <i className="fab fa-linkedin px-2" />
            </a>
            <a
              href="https://api.whatsapp.com/send?phone=+94771476691&text=Hi%20EXSOLUTE!%20I%20need%20help%20with%20"
              target="_blank"
              rel="noreferrer"
            >
              <i className="fab fa-whatsapp px-2" />
            </a>
          </div>

          <div className="col d-flex align-items-center footer-mail my-3 px-0">
            <i className="fas fa-envelope" />
            <a className="p-0 m-0 email" href="mailto:hello@exsolute.com">
              hello@exsolute.com
            </a>
          </div>

          <div className="col d-flex align-items-center footer-mobile my-3 px-0">
            <i className="fas fa-phone-alt" />
            <a className="p-0 m-0 email" href="tel:+94 771 476 691">
              +94 771 476 691
            </a>
          </div>
        </div>
      </div>
      <div className="text-center">
        <p className="pt-4">
          Copyright © 2021 | All Rights Reserved By <span>EXSOLUTE</span>
        </p>
      </div>
    </div>
  </footer>
);

export default memo(Footer);
