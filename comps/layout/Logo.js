import Image from 'next/image';

const Logo = () => (
  <div>
    <a href="/" title="thefront">
      <Image src="/images/logo.svg" alt="Picture of the author" layout="fill" className="w-100 h-100" />
    </a>
  </div>
);

export default Logo;
