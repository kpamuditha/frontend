/* eslint-disable jsx-a11y/interactive-supports-focus */
import React, { memo } from 'react';
import { Nav, Navbar } from 'react-bootstrap';
import { Link, animateScroll } from 'react-scroll';
import Scrollspy from 'react-scrollspy';

const TopBar = () => (
  <div className="header sticky-top">
    <Navbar className="container" expand="md" id="navbar">
      <button type="button" className="btn btn-link navbar-brand" onClick={() => animateScroll.scrollToTop()}>
        EXSOLUTE
      </button>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="ml-auto">
          <li className="nav-item">
            <Link activeClass="active" className="nav-link scroll-link" to="what-we-do" smooth duration={500}>
              What we do
            </Link>
          </li>
          <li className="nav-item">
            <Link activeClass="active" className="nav-link scroll-link" to="why-us" smooth duration={500}>
              Why us
            </Link>
          </li>
          <li className="nav-item">
            <Link activeClass="active" className="nav-link scroll-link" to="get-in-touch" smooth duration={500}>
              Get in touch
            </Link>
          </li>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  </div>
);

export default memo(TopBar);
